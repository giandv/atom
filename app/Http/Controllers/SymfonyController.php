<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\SymfonyScraperManager;
use App\Libraries\ValidatorManager;

class SymfonyController extends Controller
{

    public function index()
    {
        return view('symfony', []);
    }
	

    public function scrape(Request $request)
    {
        $url = NULL;
		if($request->input('url') !== NULL && strlen($request->input('url')) > 0)
			$url = $request->input('url');
		
		if(!isset($url))
			return view('symfony', ['error' => "L'url inserita non è corretta"]);
		
		try{
			$scaper_manager = new SymfonyScraperManager($url);
			$document_atom = $scaper_manager->createAtomFile($url);
			$validator_manager = new ValidatorManager($document_atom);
			if($validator_manager->isValid() != NULL)
				return view('symfony', ['error' => $validator_manager->isValid()]);
			echo $document_atom->saveXML();
			header('Content-Disposition: attachment; filename=atom.xml');
			header("Content-Type: application/force-download");
			header('Pragma: private');
			header('Cache-control: private, must-revalidate');	
		}catch(Exception $e){
			return view('symfony', ['error' => $e->getMessage()]);
		}
    }
}