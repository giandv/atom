<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\GoutteScraperManager;
use App\Libraries\ValidatorManager;

class GoutteController extends Controller
{

    public function index()
    {
        return view('goutte', []);
    }
	

    public function scrape(Request $request)
    {
        $url = NULL;
		if($request->input('url') !== NULL && strlen($request->input('url')) > 0)
			$url = $request->input('url');
		
		if(!isset($url))
			return view('goutte', ['error' => "L'url inserita non è corretta"]);
		
		try{
			$scaper_manager = new GoutteScraperManager($url);
			$document_atom = $scaper_manager->createAtomFile($url);
			$validator_manager = new ValidatorManager($document_atom);
			if($validator_manager->isValid() != NULL)
				return view('goutte', ['error' => $validator_manager->isValid()]);
			echo $document_atom->saveXML();
			header('Content-Disposition: attachment; filename=atom.xml');
			header("Content-Type: application/force-download");
			header('Pragma: private');
			header('Cache-control: private, must-revalidate');	
		}catch(Exception $e){
			return view('goutte', ['error' => $e->getMessage()]);
		}
    }
}