<?php

namespace App\Libraries;

use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\BrowserKit\HttpBrowser;

class SymfonyScraperManager {
	
	private $crawler;
	private $url;
	private $document;
	
	public function __construct($url) {
		$this->document = new \DOMDocument('1.0', 'UTF-8');
		$this->url = $url;
		$browser = new HttpBrowser(HttpClient::create());
		$this->crawler = $browser->request('GET', $url, array(), array(), array(
			'HTTP_X-Requested-With' => 'XMLHttpRequest',
		));
    }
	
	private function addSimpleField($criteria, $field_name, $element_append, $field_value = NULL){
		if(isset($criteria)){
			$filter_criteria = $this->crawler->filter($criteria);
			if(count($filter_criteria) == 1 && strlen($filter_criteria->text())){
				$element_append->appendChild($this->document->createElement($field_name,$filter_criteria->text()));
			}
		}else if(isset($field_value)){
			$element_append->appendChild($this->document->createElement($field_name,$field_value));
		}
	}
	
	private function addSimpleByContent($criteria, $field_name, $element_append, $attr_name){
		$filter_criteria = $this->crawler->filter($criteria);
		if(count($filter_criteria) == 1 && strlen($filter_criteria->text())){
			$element_append->appendChild($this->document->createElement($field_name,$filter_criteria->text()));
		}
	}
	
	private function addFieldWithAttribute($xml_element, $attr_name, $attr_value, $element_append){
		$domAttribute = $this->document->createAttribute($attr_name);
		$domAttribute->value = $attr_value;
		$xml_element->appendChild($domAttribute);
		$element_append->appendChild($xml_element);
	}

	private function getElementsByClassName($dom, $ClassName, $tagName=null) {
		if($tagName){
			$Elements = $dom->getElementsByTagName($tagName);
		}else {
			$Elements = $dom->getElementsByTagName("*");
		}
		$Matched = array();
		for($i=0;$i<$Elements->length;$i++) {
			if($Elements->item($i)->attributes->getNamedItem('class')){
				if($Elements->item($i)->attributes->getNamedItem('class')->nodeValue == $ClassName) {
					$Matched[]=$Elements->item($i);
				}
			}
		}
		return $Matched;
	}
	
	public function createAtomFile($url) {
        
		$this->document->preserveWhiteSpace = false;
		$this->document->formatOutput = true;
		$feed = $this->document->createElement('feed');
		$namespace = 'www.example.com/libraryns/1.0';
		$feed->setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns', "http://www.w3.org/2005/Atom");
		
		
		$this->addSimpleField('title','title',$feed);
		$this->addSimpleField('meta[name="description"]','sub_title',$feed);
		$this->addFieldWithAttribute($this->document->createElement('link'),'href',$url,$feed);
		
		$author = $this->document->createElement('author');
		$this->addSimpleByContent('meta[name="author"]','name',$author,'content');
		$feed->appendChild($author);
		
		$articles = $this->crawler->filter('figure');
		
		$url_link = "";
		$url_criteria = $this->crawler->filter('meta[property="og:url"]');
		if(count($url_criteria) == 1 && strlen($url_criteria->attr('content'))){
			$url_link = $url_criteria->attr('content');
			$feed->appendChild($this->document->createElement('id',$url_link));
		}
		
		foreach ($articles as $domElement) {
			$content = $domElement->C14N();
			$domDocument = new \DOMDocument();
			$domDocument->loadXml($content);
			
			$local_crawler = new Crawler($domDocument);
			$entry = $this->document->createElement('entry');
			
			$link = $local_crawler->filterXPath('//a');
			if(count($link) == 1)
				$this->addSimpleField(NULL,'link',$entry, $url_link . $link->attr('href'));
			
			$img = $local_crawler->filterXPath('//img');
			if(count($img) == 1)
				$this->addSimpleField(NULL,'img',$entry, $img->attr('src'));
			
			if(count($link) == 1 && count($img) == 1){
				$hentry_item = $this->crawler->filter('img[src="'.$img->attr('src').'"]')->closest('.hentry.hentry--item');
				if(count($hentry_item) == 1){					
					$data_id = $hentry_item->attr('data-id');
					$this->addSimpleField(NULL,'data-id',$entry, $data_id);
					$this->addSimpleField(NULL,'data-timesec',$entry, $hentry_item->attr('data-timesec'));
					$this->addSimpleField(NULL,'data-datetime',$entry, $hentry_item->attr('data-datetime'));
					
					$entry->appendChild($this->document->createElement('meta_time',
						$this->crawler->filter('.hentry.hentry--item[data-id="'.$data_id.'"] .meta-time')->text())
					);
				}
			}
			$feed->appendChild($entry);
		}
		
		$this->document->appendChild($feed);
		return $this->document;
	}

}