<?php

namespace App\Libraries;

class ValidatorManager {
	
	private $xml;
	
	public function __construct($document) {
		$this->xml = $document->saveXML($document->documentElement);
    }
	
	public function isValid(){
		$xml = new \XMLReader();
		if (!$xml->xml($this->xml, NULL, LIBXML_DTDVALID)) {
		  return "XML not valid: load error";
		}

		libxml_use_internal_errors(TRUE);

		$arErrors = libxml_get_errors();
		$xml_errors = "";
		foreach ($arErrors AS $xmlError) $xml_errors .= $xmlError->message;
		if ($xml_errors != "") {
		  return "XML not valid: ".$xml_errors;
		}
		return NULL;
	}
	
}