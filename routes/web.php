<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('goutte', ['uses' => 'GoutteController@index']);
Route::post('gouttecontroller', 'GoutteController@scrape');

Route::get('symfony', ['uses' => 'SymfonyController@index']);
Route::post('symfonycontroller', 'SymfonyController@scrape');